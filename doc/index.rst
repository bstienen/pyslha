Welcome to PySLHA's documentation!
==================================

.. toctree::
   :maxdepth: 2

This is the documentation for the PySLHA package for reading, writing, and
manipulating particle mass spectra, decays, and cross-sections in beyond
Standard Model (BSM) physics scenarios via the SUSY Les Houches Accord (SLHA)
format.

PySLHA is a Python package for handling SLHA files and logical content in a neat
and powerful way, for plotting BSM model spectra and decay channels via the
`slhaplot` utility, and to a limited extent for converting to and from the
legacy ISAWIG spectrum+decay format. As of PySLHA version 3.1.0, the full SLHA3
standard, including `XSECTION` blocks, is supported.

This documentation is based largely on the docstrings embedded in the Python
code, so the documentation you see here is the same as the documentation you can
get by calling `pydoc` or by using the `help()` function in the Python
interpreter, e.g.::

    $ python
    >>> import pyslha
    >>> help(pyslha)

Feel free to contact me at andy@insectnation.org if you find things I can
improve either in the documentation or the package itself.

Installing
==========

PySLHA is very easy to install. Download and expand the source code tarball/zip,
and `cd` into the main directory. To install, use the Python standard install procedure, i.e.::

    $ python setup.py install --prefix=$HOME/some/dir

The `--prefix ...` bit is optional, but probably you will want it in order to
install the library and programs into user space (i.e. your home directory)
rather than a system location like `usr/local`.

I use `$HOME/local` or `$HOME/heplocal` for this purpose, and just make sure
that the appropriate `bin`, `lib`, and `lib/python2.x/site-packages`
sub-directories are listed in my `PATH`, `LD_LIBRARY_PATH`, and `PYTHONPATH`
environment variables respectively. You may need to set these variables before
installing, since IIRC the Python distutils `install` command will complain if
you try to install Python modules into a dir _not_ in the `PYTHONPATH`.


`slhaplot`
==========

Many users will only be interested in PySLHA to the extent that they want to
plot SUSY or other model spectra and decay channels via the `slhaplot`
tool. Using `slhaplot` requires a functional LaTeX installation with the `tikz`
package available for all output other than the `tex` format; bitmap formats
derived from LaTeX's PDF output may require other tools such as `convert` from ImageMagick.

`slhaplot` is very easy to use in the most basic mode: ::

    $ slhaplot some_model.slha

which will result in a `some_model.pdf` file. The `-f` optional flag can be used to change this format, e.g.::

    $ slhaplot some_model.slha -f png

will produce a PNG bitmap image. Multiple formats can be produced at once --
call `slhaplot --help` to get full help documentation.  Here's an example of a
mass spectrum plot made this way:

.. image:: softsusy.*

The `--br` flag can be used to specify a minimum branching ratio: if given,
decay channel arrows will be drawn for all decays with a BR greater than this number. For example,::

    $ slhaplot some_model.slha --br=0.1

will produce something like:

.. image:: sps1a.*

There are many more options for style tweaking, and for those who want total
control you can use `-f tex` to write a raw LaTeX source file which can then be
edited by hand and seamlessly incorporated into your document.


PySLHA API
==========

.. toctree::
   :maxdepth: 2


.. automodule:: pyslha
   :members:


ISAWIG conversion
=================

The scripts `isawig2slha` and `slha2isawig` are mini frontends on the API
functions for reading and writing from/to ISAWIG files as well as SLHA. They may
be useful for legacy model/generator purposes. For details, please run
`isawig2slha --help` or `slha2isawig --help` depending on the direction in which
you need to make a conversion. We believe these conversion functions to be 99%
complete, but three-body decays may not be fully implemented -- please let me
know if you encounter a problem, and particularly if you can supply a solution!


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
